import scrypt
from base58 import b58encode_int as base58e
from os import urandom

import constants as const

def new_pass(password):
    return scrypt.encrypt(str(urandom(64)), password, maxtime=const.MAX_SCRYPT_TIME)

def check_pass(guess, dbpass):
    try:
        scrypt.decrypt(dbpass, guess, maxtime=const.MAX_SCRYPT_TIME)
        return True
    except scrypt.error:
        return False

def new_uid():
    uid = ''
    while len(uid) < const.UID_LEN:
        uid = str(int.from_bytes(urandom(const.UID_LEN), 'big'))
    return uid[:const.UID_LEN]

def new_small_id():
    small_id = ''
    while len(small_id) < const.SMALL_ID_LEN:
        small_id = base58e(int.from_bytes(urandom(const.SMALL_ID_BYTES), 'big')).decode()
    return small_id[:const.SMALL_ID_LEN]

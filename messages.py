import json


root = 'Henlo hooman owo\nDatabase good? {}'

reg_error = json.dumps(
{'message' : 'Invalid credentials provided',
'data' : [],
'error' : 1})

new_user_success = json.dumps(
{'message' : 'New user registration complete, welcome to the club owo',
'data' : [],
'error' : 0})

login_success = json.dumps(
{'message' : 'Login was successful',
'data' : [],
'error' : 0})

login_fail = json.dumps(
{'message' : 'Failed to login',
'data' : [],
'error' : 1})


'''
json.dumps(
{'message' : '',
'data' : [],
'error' : 0})
'''

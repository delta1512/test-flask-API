from flask import Flask, request, render_template, make_response, g
import mysql.connector as cnx
from time import time

import crypt
import conf as c
import messages as m
import queries as qs

app = Flask(__name__)


### FUNCTIONS
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = cnx.connect(host=c.DB_HOST, user=c.DB_USER, passwd=c.DB_PASS, db=c.DB_NAME)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def q_ro(query, args=(), one=False):
    cur = get_db().cursor()
    cur.execute(query, args)
    result = cur.fetchall()
    cur.close()
    return (result[0] if result else None) if one else result


def q_wo(query, args=()):
    cur = get_db().cursor()
    cur.execute(query, args)
    cur.close()
    get_db().commit()
###


### API DIRECTORIES
@app.route('/')
def deltas_site():
    return m.root.format(q_ro(qs.check_db, one=True) is not None)


@app.route('/index')
def index():
    if request.cookies.get('expiry') is not None:
        td = float(request.cookies.get('expiry'))-time()
        if td > 10:
            resp = make_response(render_template('index.html'))
            resp.set_cookie('expiry', value=str(time()+10).encode(), expires=time()+10)
        else:
            resp = f'<marquee>Please wait {td} seconds</marquee>'
    else:
        resp = make_response(render_template('index.html'))
        resp.set_cookie('expiry', value=str(time()+10).encode(), expires=time()+10)
    return resp


@app.route('/register')
def register():
    uname = request.args.get('uname', None)
    email = request.args.get('email', None)
    passw = request.args.get('pass', None)
    if any(map(lambda x: x is None, [uname, email, passw])):
        return m.reg_error
    hpass = crypt.new_pass(passw)
    q_wo(qs.new_user, args=(crypt.new_uid(), round(time()), 0, uname, hpass, 0, crypt.new_small_id(), email))
    return m.new_user_success


@app.route('/login')
def login():
    email = request.args.get('email', None)
    passw = request.args.get('pass', None)
    if any(map(lambda x: x is None, [email, passw])):
        return m.reg_error
    dbpass = q_ro(qs.user_login, args=(email), one=True)
    if crypt.check_pass(passw, dbpass):
        return m.login_success
    else:
        return m.login_fail


@app.route('/text_delta', methods=['GET', 'POST'])
def text_delta():
    if request.method == 'POST':
        q_wo(qs.send_msg, args=(request.form['user'], request.form['message']))
        return 'Message sent'
    else:
        return render_template('textme.html')
###
